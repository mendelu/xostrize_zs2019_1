#include <iostream>

using namespace std;

class Balik{
public:
    string m_odesilatel;
    string m_prijemce;
    float m_vaha;
    float m_vyska;
    float m_sirka;
    float m_hloubka;
    string m_datumPrevzeti;
    string m_posledniZnamaLokace;

    Balik(string prijemce, float vaha, float vyska, float sirka, float hloubka){
        m_prijemce= prijemce;
        m_vaha = vaha;
        setVyska(vyska);
        m_sirka= sirka;
        m_hloubka = hloubka;
        m_odesilatel="";
        m_datumPrevzeti="dnes";
        m_posledniZnamaLokace = "tady";
        cout<< "balik prevzat do evidence" << endl;
    }

    void setVyska(float vyska){
        if(vyska>0){
            m_vyska = vyska;
        }else{
            cout<< "zadavas spatnou vysku automaticky nastavuji 0"<< endl;
            m_vyska = 0
        }
    }

    Balik(string prijemce, string odesilatel, float vaha, float vyska, float sirka, float hloubka){
        m_prijemce= prijemce;
        m_vaha = vaha;
        setVyska(vyska);
        m_sirka= sirka;
        m_hloubka = hloubka;
        m_odesilatel="";
        m_datumPrevzeti="dnes";
        m_posledniZnamaLokace = "tady";
        cout<< "balik prevzat do evidence" << endl;
        m_odesilatel= odesilatel;
    }

    float vypoctiObjem(){
        return m_sirka*m_vyska*m_hloubka;
    }

    void setLokace(string novaLokace){
        m_posledniZnamaLokace = novaLokace;
    }
    void printInfo(){
        //novy radek v retezci je \n
        //tabulator v retezci je \t
        //priklad cout<<"tohle bude na prvnim radku \n tohle bude na druhem \t tohle bude odsazeny << endl;"
        cout<<"Balik ----------------------" << endl;
        cout<<"Odesilatel: " << m_odesilatel << endl;
        cout<<"Prijemce: " << m_prijemce << endl;
        cout<<"Objem baliku:" << vypoctiObjem() << endl;
        cout<<"rozmery [s v h]"<< m_sirka << " " << m_vyska << " " << m_hloubka << endl;
        cout<<"Datum prevzeti:" << m_datumPrevzeti << endl;
        cout<<"Posledni znama lokace:" << m_posledniZnamaLokace << endl;

    }

    ~Balik(){
        cout<< "Balik dorucen \n neni ho jiz treba drzet v evidenci" << endl;
    }
};

int main() {
    Balik* postovniBalik = new Balik("Frantisek Ostrizek", 0.5, 2.5,1,1);

    postovniBalik->printInfo();

    postovniBalik->setLokace("Brno, Zemedelska 1");

    postovniBalik->printInfo();

    delete postovniBalik;

    Balik* pplBalik = new Balik("Ondrej Svehla", "Frantisek Ostrizek", 0.3, 0.8, 5,1);

    pplBalik ->printInfo();

    delete(pplBalik);

    return 0;
}