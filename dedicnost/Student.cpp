//
// Created by xostrize on 15.11.2019.
//

#include "Student.h"


Student::Student(string jmeno, string rodneCislo, int semestr, float prumer):Osoba(jmeno,rodneCislo){
    setPrumer(prumer);
    m_semestr=semestr;
}
void Student::zvysSemestr(){
    m_semestr++;
}
int Student::getSemestr(){
    return m_semestr;
}
float Student::getPrumer(){
    return m_prumer;
}
void Student::setPrumer(float prumer){
    if(prumer>0){
        m_prumer=prumer;
    }else{
        cout<< "prumer nemuze byt zaporny" << endl;
        m_prumer=0;
    }
}
void Student::printInfo(){
    Osoba::printInfo();
    cout<<"Student ------------" << endl;
    cout<< "semestr: \t" << getSemestr() << endl;
    cout<< "prumer: \t" << getPrumer() << endl;
}