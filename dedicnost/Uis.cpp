//
// Created by xostrize on 15.11.2019.
//

#include "Uis.h"

Uis::Uis(){

}
void Uis::addStudent(string jmeno, string rodneCislo, int semestr, float prumer){
    m_studenti.push_back(new Student(jmeno,rodneCislo,semestr,prumer));
}
void Uis::addUcitel(string jmeno, string rodneCislo, string ustav){
    m_ucitele.push_back(new Ucitel(jmeno, rodneCislo, ustav));
}
void Uis::printInfo(){
    cout<< "Studenti ###############" << endl;
    for(Student* st: m_studenti){
        st->printInfo();
    }
    cout<< "Ucitele ###############" << endl;
    for(Ucitel* uc: m_ucitele){
        uc->printInfo();
    }
}
Uis::~Uis(){
    for(Student* st: m_studenti){
        delete st;
    }
    for(Ucitel* uc: m_ucitele){
        delete uc;
    }
}