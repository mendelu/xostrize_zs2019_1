#include <iostream>
#include "Osoba.h"
#include "Ucitel.h"
#include "Student.h"
#include "Uis.h"

/***
 * Modelujme informační systém školy, kde budou evidováni učitelé a studenty.

Každý student bude ponese jmeno, rodné číslo, semestr ve kterém studuje a studijní průměr. Všechny potřebné údaje budou vynuceny konstruktorem a budou modifikovány příslušnými getry a setry. Mějme metodu printInfo, která nám bude sloužit jako kontrolní výpis.

Každý učitel vlastní jméno, rodné číslo a ústav na kterém pracuje. Všechny potřebné údaje budou vynuceny konstruktorem a budou modifikovány příslušnými getry a setry. Mějme metodu printInfo, která nám bude sloužit jako kontrolní výpis.

Informační systém školy bude združovat učitele a studenty. Bude schopen je vypsat do konzole, přidávat a odebírat.
 *
 * @return 0 když program skončí korektně
 */

int main() {
    Osoba* osoba= new Osoba("Frantisek","123456789");
    osoba->printInfo();
    delete(osoba);

    Ucitel* ucitel = new Ucitel("Mira", "987654321","Department of informatics");
    ucitel->printInfo();
    delete(ucitel);

    Student* student= new Student("Jakub", "654987321", 1, 1.5);
    student->printInfo();
    delete(student);


    Osoba* osoba1 = new Ucitel("Tomas", "1478963", "Department of statistics");
    osoba1->printInfo();
    delete(osoba1);




    cout<<"Uis ######################" <<endl;

    Uis* uis = new Uis();
    uis->addStudent("Jakub", "654987321", 1, 1.5);
    uis->addUcitel("Tomas", "1478963", "Department of statistics");
    uis->printInfo();
    delete(uis);

    return 0;
}