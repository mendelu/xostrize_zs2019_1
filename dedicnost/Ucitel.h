//
// Created by xostrize on 15.11.2019.
//

#ifndef DEDICNOST_UCITEL_H
#define DEDICNOST_UCITEL_H

#include <iostream>
#include "Osoba.h"

using namespace std;

class Ucitel: public Osoba{
private:
    string m_ustav;
public:
    Ucitel(string jmeno, string rodneCislo, string ustav);
    string getUstav();
    void setUstav(string ustav);
    void printInfo();
};


#endif //DEDICNOST_UCITEL_H
