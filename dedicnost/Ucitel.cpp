//
// Created by xostrize on 15.11.2019.
//

#include "Ucitel.h"

Ucitel::Ucitel(string jmeno, string rodneCislo, string ustav):Osoba(jmeno, rodneCislo){
    setUstav(ustav);
}
string Ucitel::getUstav(){
    return m_ustav;
}
void Ucitel::setUstav(string ustav){
    m_ustav=ustav;
}
void Ucitel::printInfo(){
    Osoba::printInfo();
    cout<<"Ucitel ------------------" << endl;
    cout<<"Ustav: \t" << getUstav() << endl;
}