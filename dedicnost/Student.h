//
// Created by xostrize on 15.11.2019.
//

#ifndef DEDICNOST_STUDENT_H
#define DEDICNOST_STUDENT_H

#include <iostream>
#include "Osoba.h"

using namespace std;
class Student: public Osoba {
private:
    int m_semestr;
    float m_prumer;
public:
    Student(string jmeno, string rodneCislo, int semestr, float prumer);
    void zvysSemestr();
    int getSemestr();
    float getPrumer();
    void setPrumer(float prumer);
    void printInfo();
};


#endif //DEDICNOST_STUDENT_H
