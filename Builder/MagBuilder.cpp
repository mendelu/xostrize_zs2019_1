//
// Created by xostrize on 13.12.2019.
//

#include "MagBuilder.h"

void MagBuilder::createHrdina(string jmeno) {
    m_hrdina = new Hrdina(jmeno, 10,10);
}

void MagBuilder::generujZbrane() {
    m_hrdina ->seberZbran(new Zbran("mec", 10));
    m_hrdina ->seberZbran(new Zbran("hul", 10));

}

void MagBuilder::generujLektvary() {
    m_hrdina->seberLektvar(new Lektvar(10,10));
}
