//
// Created by xostrize on 13.12.2019.
//

#ifndef BUILDER_HRDINA_H
#define BUILDER_HRDINA_H

#include <iostream>
#include <vector>
#include "Zbran.h"
#include "Lektvar.h"

using namespace std;
class Hrdina {
private:
    string m_jmeno;
    int m_sila;
    int m_obrana;

    vector<Zbran*> m_zbrane;
    vector<Lektvar*> m_lektvary;
public:
    Hrdina(string jmeno, int sila, int obrana);
    int getUtok();
    int getObrana();
    void seberLektvar(Lektvar* lektvar);
    void seberZbran(Zbran* zbran);
    void vypijPosledni();
    void printInfo();


};


#endif //BUILDER_HRDINA_H
