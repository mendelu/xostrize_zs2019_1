//
// Created by xostrize on 13.12.2019.
//

#include "Hrdina.h"


Hrdina::Hrdina(string jmeno, int sila, int obrana){
    m_jmeno=jmeno;
    m_sila=sila;
    m_obrana=obrana;
}
int Hrdina::getUtok(){
    int soucet = 0;
    for(Zbran* zb: m_zbrane){
        soucet += zb->getBonusSily();
    }

    return m_sila+soucet;
}
int Hrdina::getObrana(){
    return m_obrana;
}
void Hrdina::seberLektvar(Lektvar* lektvar){
    m_lektvary.push_back(lektvar);
}
void Hrdina::seberZbran(Zbran* zbran){
    m_zbrane.push_back(zbran);
}
void Hrdina::vypijPosledni(){
    if(m_lektvary.size()>0) {
        m_sila += m_lektvary.at(m_lektvary.size() - 1)->getBonusSily();
        m_obrana += m_lektvary.at(m_lektvary.size() - 1)->getBonusObrany();
        m_lektvary.pop_back();
    }
}
void Hrdina::printInfo(){
    cout<< "###### Hrdina #####" << endl;
    cout<< "jmeno:\t"<< m_jmeno<< endl;
    cout<< "Utok: \t"<< getUtok() << endl;
    cout<< "Obrana:\t" << getObrana() << endl;
    cout<< "pocet lektvaru: " << m_lektvary.size() << endl;
    cout<< "------ arzenal -----" << endl;
    for(Zbran* zb:m_zbrane){
        cout<< zb->getJmeno() << endl;
    }
    cout<< "--------------------" << endl;

}