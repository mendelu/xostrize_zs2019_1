#include <iostream>
#include "Hrdina.h"
#include "HrdinaBuilder.h"
#include "HrdinaDirector.h"
#include "MagBuilder.h"

int main() {

    HrdinaDirector* director = new HrdinaDirector(new MagBuilder());

    Hrdina* carodej = director->createhrdina("Gandalf");

    carodej->printInfo();

    delete(director);
    delete(carodej);

    return 0;
}