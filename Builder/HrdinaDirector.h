//
// Created by xostrize on 13.12.2019.
//

#ifndef BUILDER_HRDINADIRECTOR_H
#define BUILDER_HRDINADIRECTOR_H

#include "HrdinaBuilder.h"
#include "Hrdina.h"

class HrdinaDirector {
private:
    HrdinaBuilder* m_builder;
public:
    HrdinaDirector(HrdinaBuilder* builder);
    void setBuilder(HrdinaBuilder* builder);
    Hrdina* createhrdina(string jmeno);
};


#endif //BUILDER_HRDINADIRECTOR_H
