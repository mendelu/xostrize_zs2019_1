//
// Created by xostrize on 13.12.2019.
//

#include "HrdinaDirector.h"

HrdinaDirector::HrdinaDirector(HrdinaBuilder* builder){
    m_builder=builder;
}
void HrdinaDirector::setBuilder(HrdinaBuilder* builder){
    m_builder=builder;
}
Hrdina* HrdinaDirector::createhrdina(string jmeno){
    m_builder->createHrdina(jmeno);
    m_builder->generujLektvary();
    m_builder->generujZbrane();
    return m_builder->getHrdina();
}