//
// Created by xostrize on 13.12.2019.
//

#ifndef BUILDER_LEKTVAR_H
#define BUILDER_LEKTVAR_H


class Lektvar {
private:
    int m_bonusSily;
    int m_bonusObrany;

public:

    Lektvar(int sila, int obrana);
    int getBonusSily();
    int getBonusObrany();
};


#endif //BUILDER_LEKTVAR_H
