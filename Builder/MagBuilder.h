//
// Created by xostrize on 13.12.2019.
//

#ifndef BUILDER_MAGBUILDER_H
#define BUILDER_MAGBUILDER_H

#include "HrdinaBuilder.h"

class MagBuilder: public HrdinaBuilder {
public:
    void createHrdina(string jmeno) override;

    void generujZbrane() override;

    void generujLektvary() override;
};


#endif //BUILDER_MAGBUILDER_H
