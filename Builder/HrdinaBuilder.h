//
// Created by xostrize on 13.12.2019.
//

#ifndef BUILDER_HRDINABUILDER_H
#define BUILDER_HRDINABUILDER_H

#include "Hrdina.h"

class HrdinaBuilder {
protected:
    Hrdina* m_hrdina;
public:
    virtual void createHrdina(string jmeno) =0;
    virtual void generujZbrane() =0;
    virtual void generujLektvary() = 0;
    HrdinaBuilder();
    Hrdina* getHrdina();
};


#endif //BUILDER_HRDINABUILDER_H
