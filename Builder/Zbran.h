//
// Created by xostrize on 13.12.2019.
//

#ifndef BUILDER_ZBRAN_H
#define BUILDER_ZBRAN_H

#include <iostream>

using namespace std;

class Zbran {
private:
    string m_jmeno;
    int m_bonusSily;
public:
    Zbran(string jmeno, int bonus);
    string getJmeno();
    int getBonusSily();
};


#endif //BUILDER_ZBRAN_H
