//
// Created by xostrize on 01.11.2019.
//

#include "Sklad.h"

Sklad::Sklad(){
    m_patra.push_back(new Patro("Prizemi"));
    m_pocitadloPater=0;
}
Sklad::~Sklad(){
    for(Patro* patr:m_patra){
        delete(patr);
    }
}
void Sklad::postavPatro(){
    m_pocitadloPater++;
    m_patra.push_back(new Patro(to_string(m_pocitadloPater)));
}
void Sklad::ubourejPatro(){
    delete(m_patra.at(m_patra.size()-1));
    m_patra.pop_back();
}
void Sklad::ulozKontejner(int patro, int pozice, Kontejner* kont){}
Kontejner* Sklad::odeberKontejener(int patro, int pozice){
    return nullptr;
}
void Sklad::vypisObsah(){
    cout<< "Sklad------------------" << endl;
    for(Patro* patr: m_patra){
        patr->vytiskniObsah();
    }
}