//
// Created by xostrize on 01.11.2019.
//

#include "Kontejner.h"


Kontejner::Kontejner(string majitel, float vaha, float objem) {
 m_majitel=majitel;
 m_vaha=vaha;
 m_objem=objem;
}

float Kontejner::getVaha() {
    return m_vaha;
}

float Kontejner::getObjem() {
    return m_objem;
}

string Kontejner::getMajitel(){
        return m_majitel;
}