//
// Created by xostrize on 01.11.2019.
//

#include "Patro.h"


Patro::Patro(string oznaceni){
    m_oznaceni=oznaceni;

    for(Kontejner* &kont: m_pozice){
        kont= nullptr;
    }
}
void Patro::ulozKontejner(int pozice, Kontejner* kontejner){
    if(pozice >=0 and pozice <=10){
        m_pozice.at(pozice)=kontejner;
    }else{
        cout<< "snazis se pristoupit mimo rozsah patra" << endl;
    }
}
Kontejner* Patro::odeberKontejner(int ktery){
    if(ktery >=0 and ktery <=10){
        Kontejner* kont=m_pozice.at(ktery);
        m_pozice.at(ktery)= nullptr;
        return kont;
    }else{
        cout<< "snazis se pristoupit mimo rozsah patra" << endl;
        return nullptr;
    }
}
void Patro::vytiskniObsah(){
 cout<< "Tisk patroveho stani " <<m_oznaceni<<" -------------------------"<< endl;
    for(int i =0; i< m_pozice.size();i++){
        cout<<"Kontejnerove stani "<< i << "\t";
        if(m_pozice.at(i)== nullptr){
            cout<< "Prazdne misto"<< endl;
        }else{
            cout<< endl;
            cout<<"Majitel: \t"<< m_pozice.at(i)->getMajitel() << endl;
            cout<<"Objem: \t"<< m_pozice.at(i)->getObjem() << endl;
            cout<<"Vaha: \t"<< m_pozice.at(i)->getVaha() << endl;
        }
    }
}