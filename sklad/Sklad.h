//
// Created by xostrize on 01.11.2019.
//

#ifndef SKLAD_SKLAD_H
#define SKLAD_SKLAD_H

#include <vector>
#include <iostream>
#include "Patro.h"
#include "Kontejner.h"

using namespace std;

class Sklad {

private:
    vector<Patro*> m_patra;
    int m_pocitadloPater;
public:

    Sklad();
    ~Sklad();
    void postavPatro();
    void ubourejPatro();
    void ulozKontejner(int patro, int pozice, Kontejner* kont);
    Kontejner* odeberKontejener(int patro, int pozice);
    void vypisObsah();

};


#endif //SKLAD_SKLAD_H
