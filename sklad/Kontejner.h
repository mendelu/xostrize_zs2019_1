//
// Created by xostrize on 01.11.2019.
//

#ifndef SKLAD_KONTEJNER_H
#define SKLAD_KONTEJNER_H

#include <iostream>

using namespace std;

class Kontejner {
private:
    float m_objem;
    float m_vaha;
    string m_majitel;
public:

    Kontejner(string majitel, float vaha, float objem);
    float getVaha();
    float getObjem();
    string getMajitel();
};


#endif //SKLAD_KONTEJNER_H
