#include <iostream>

#include "Kniha.h"
#include "Knihovna.h"

int main() {

    Knihovna* knihovna = Knihovna::createKnihovna();
    knihovna->addBook(new Kniha("Christopher Paoliny", "Inhertance"));
    knihovna->addBook(knihovna->createBook( "Eragon", "Christopher Paoliny"));

    knihovna->printInfo();

    cout<< "###############################################" << endl;

    knihovna->searchForAutor("Christopher Paoliny");

    cout<< "###############################################" << endl;

    knihovna->removeBook(1);

    knihovna->printInfo();



    delete(knihovna);
    return 0;
}