//
// Created by xostrize on 08.11.2019.
//

#ifndef KNIHOVNA_KNIHA_H
#define KNIHOVNA_KNIHA_H

#include <iostream>

using namespace std;


class Kniha {
private:
    string m_autor;
    string m_nazev;
    int m_id_knihy;
    static int s_id;
public:
    Kniha(string autor, string nazev);
    void printInfo();
    string getAutor();
    int getId();
};

#endif //KNIHOVNA_KNIHA_H
