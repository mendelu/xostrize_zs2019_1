//
// Created by xostrize on 08.11.2019.
//

#include "Knihovna.h"

int Knihovna::s_id=0;

Knihovna::Knihovna(){
    s_id++;
    m_id_Knihovny=s_id;
}
Knihovna* Knihovna::createKnihovna(){
    return new Knihovna();
}
void Knihovna::printInfo(){
    cout<<"Knihovana ------------- "<< m_id_Knihovny << endl;
    for(Kniha* kn:m_knihy){
        kn->printInfo();
    }

}
Kniha* Knihovna::createBook(string nazev, string autor){
    return new Kniha(autor, nazev);
}
void Knihovna::addBook(Kniha* kniha){
    m_knihy.push_back(kniha);
}
void Knihovna::searchForAutor(string autor){
    cout<< "vyhledavam knihy autora \t" << autor << endl;
    for(Kniha* kn: m_knihy){
        if(kn->getAutor()==autor){
            kn->printInfo();
        }
    }
}
void Knihovna::removeBook(int id){
    for(int i = 0; i< m_knihy.size(); i++){
        if(m_knihy.at(i)->getId()==id){
            delete(m_knihy.at(i));
            m_knihy.erase(m_knihy.begin()+i);
        }
    }
}