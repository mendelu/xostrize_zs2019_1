//
// Created by xostrize on 08.11.2019.
//

#include "Kniha.h"

int Kniha::s_id = 0;

Kniha::Kniha(string autor, string nazev){
    m_nazev=nazev;
    m_autor=autor;
    s_id++;
    m_id_knihy=s_id;
}

int Kniha::getId(){
    return m_id_knihy;
}
void Kniha::printInfo(){
    cout<<"Kniha--------------------" << endl;
    cout<< "id knihy:\t"<< m_id_knihy<< endl;
    cout<< "nazev: \t"<< m_nazev<< endl;
    cout<< "autor: \t"<< getAutor()<< endl;
}
string Kniha::getAutor(){
    return m_autor;
}