//
// Created by xostrize on 08.11.2019.
//

#ifndef KNIHOVNA_KNIHOVNA_H
#define KNIHOVNA_KNIHOVNA_H

#include <iostream>
#include <vector>

#include "Kniha.h"

using namespace std;

class Knihovna {
private:
    vector<Kniha*> m_knihy;
    int m_id_Knihovny;
    static int s_id;

    Knihovna();
public:
    static Knihovna* createKnihovna();
    void printInfo();
    Kniha* createBook(string nazev, string autor);
    void addBook(Kniha* kniha);
    void searchForAutor(string autor);
    void removeBook(int id);
};


#endif //KNIHOVNA_KNIHOVNA_H
