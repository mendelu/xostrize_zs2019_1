#include <iostream>
#include "Automat.h"
#include "Napoj.h"

using namespace std;

/*
 *  Vytvořte informační systém Nápojového Napoju, který eviduje Nápoje a Mince.
 *  Každý Nápoj obsahuje textový popis zboží, objem (des. číslo) a cenu (celé číslo).
 *  Všechny tyto informace jsou povinné (vynucené při vytvoření Nápoje), ale může se stát, že obsah je neznámý.
 *  V takovém případě se pouze zadá Cena napoje a objem je nastaven na 0, popis obsahu je "neznámé". (1 body vlast., 2 body konst.)
 *  Dále program obsahuje třídu Napoj, který představuje kontejner do kterého se ukládají Nápoje.
 *  Automat má vlastnosti celkový objem a celkový počet kusů zboží.
 *  Na počátku jsou oba nastaveny na 0. (1 bod konst.)
 *  Třída obsahuje metodu uložNápoj, která převezme ukazatel na Napoj a přidá do celkového objemu a počtu kusů hodnoty z Nápoje. (1 bod)
 *  Dále třída obsahuje metodu odeberNápoj, která opět převezme ukazatel na Napoj a odečte z celkových hodnot hodnoty Nápoje.
 *  Proběhne však kontrola a pokud je v kontejneru menší objem nebo menší počet kusů zboží, než je uvedeno v kontejneru, odečet se neprovede. (2 body)
 *  Poslední metodou je metoda printInfo vypisující obsah kontejneru na obrazovku (1 bod).
 *  V hlavní funkci programu vytvořte Napoj a dva nápoje. Ty vložte do Napoju (jeden pomocí parametru, druhý odkazem) a vypište jeho stav. (2 body)
 * */


int main(int argc, const char * argv[]) {

    //V hlavní funkci programu vytvořte Napoj a dva nápoje. Ty vložte do Napoju (jeden pomocí parametru, druhý odkazem) a vypište jeho stav. (2 body)
    Automat *a1 = new Automat();
    Napoj *napoj1 = new Napoj("limca", 7.2, 1);
    Napoj *napoj2 = new Napoj(2);
    a1->storeNapoj(napoj1);
    a1->storeNapoj(napoj2);
    a1->printInfo();
    delete napoj1;
    delete napoj2;
    delete a1;
    return 0;
}