//
// Created by xostrize on 25.10.2019.
//

#ifndef AUTOMAT_NAPOJ_H
#define AUTOMAT_NAPOJ_H

#import <iostream>

using namespace std;

class Napoj {
private:
/*
    *  Vytvořte informační systém Nápojového Napoju, který eviduje Nápoje a Mince.
    *  Každý Nápoj obsahuje textový popis zboží, objem (des. číslo) a cenu (celé číslo).
    */
    string m_content;
    float m_objem;
    int m_cena;

public:
    /*
    * Všechny tyto informace jsou povinné (vynucené při vytvoření Nápoje), ale může se stát, že obsah je neznámý.
    *  V takovém případě se pouze zadá Cena napoje a objem je nastaven na 0, popis obsahu je "neznámé". (1 body vlast., 2 body konst.)
     *  */
    Napoj(string content, float objem, int cena);
    /*
    *  V takovém případě se pouze zadá Cena napoje a objem je nastaven na 0, popis obsahu je "neznámé". (1 body vlast., 2 body konst.)
    *  */
    Napoj(float cena);

    int getObjem();

    float getCena();

    string getContent();
};


#endif //AUTOMAT_NAPOJ_H
