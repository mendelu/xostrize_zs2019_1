//
// Created by xostrize on 25.10.2019.
//

#include "Napoj.h"


/*
* Všechny tyto informace jsou povinné (vynucené při vytvoření Nápoje), ale může se stát, že obsah je neznámý.
*  V takovém případě se pouze zadá Cena napoje a objem je nastaven na 0, popis obsahu je "neznámé". (1 body vlast., 2 body konst.)
 *  */
Napoj::Napoj(string
             content,
             float objem,
             int cena
) {
    m_content = content;
    m_objem = objem;
    m_cena = cena;
}

/*
*  V takovém případě se pouze zadá Cena napoje a objem je nastaven na 0, popis obsahu je "neznámé". (1 body vlast., 2 body konst.)
*  */
Napoj::Napoj(float cena) {
    m_content = "unknown";
    m_objem = 0;
    m_cena = cena;
}

int Napoj::getObjem() {
    return m_objem;
}

float Napoj::getCena() {
    return m_cena;
}

string Napoj::getContent() {
    return m_content;
}