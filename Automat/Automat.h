//
// Created by xostrize on 25.10.2019.
//

#ifndef AUTOMAT_AUTOMAT_H
#define AUTOMAT_AUTOMAT_H

#include <iostream>
#include "Napoj.h"
using  namespace std;
class Automat {
private:
    // Automat má vlastnosti celkový objem a celkový počet kusů zboží.
    float m_totalObjem;
    int m_totalCena;
    int m_pocetKusu;
public:
    //Na počátku jsou oba nastaveny na 0. (1 bod konst.)
    Automat();
//Třída obsahuje metodu uložNápoj, která převezme ukazatel na Napoj a přidá do celkového objemu a počtu kusů hodnoty z Nápoje. (1 bod)
    void storeNapoj(Napoj* storedNapoj);

    // optionaly:

    void pridejNapoj(float objem,int  cena);
    /*
     * Dále třída obsahuje metodu odeberNápoj, která opět převezme ukazatel na Napoj a odečte z celkových hodnot hodnoty Nápoje.
 *  Proběhne však kontrola a pokud je v kontejneru menší objem nebo menší počet kusů zboží, než je uvedeno v kontejneru, odečet se neprovede. (2 body)
     * */
    void removeNapoj(Napoj* storedNapoj);

    /*
     *  Poslední metodou je metoda printInfo vypisující obsah kontejneru na obrazovku (1 bod).
     * */
    void printInfo();
};


#endif //AUTOMAT_AUTOMAT_H
