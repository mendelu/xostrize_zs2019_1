//
// Created by xostrize on 25.10.2019.
//

#include "Automat.h"

//Na počátku jsou oba nastaveny na 0. (1 bod konst.)
Automat::Automat(){
    m_totalObjem = 0;
    m_totalCena = 0;
    m_pocetKusu = 0;
}
//Třída obsahuje metodu uložNápoj, která převezme ukazatel na Napoj a přidá do celkového objemu a počtu kusů hodnoty z Nápoje. (1 bod)
void Automat::storeNapoj(Napoj* storedNapoj){
    m_totalObjem += storedNapoj->getObjem();
    m_totalCena += storedNapoj->getCena();
    m_pocetKusu++;
}

// optionaly:

void Automat::pridejNapoj(float objem,int  cena){
    m_totalObjem += objem;
    m_totalCena += cena;
    m_pocetKusu++;
}



/*
 * Dále třída obsahuje metodu odeberNápoj, která opět převezme ukazatel na Napoj a odečte z celkových hodnot hodnoty Nápoje.
*  Proběhne však kontrola a pokud je v kontejneru menší objem nebo menší počet kusů zboží, než je uvedeno v kontejneru, odečet se neprovede. (2 body)
 * */
void Automat::removeNapoj(Napoj* storedNapoj){
    if (storedNapoj ->getObjem() < m_totalObjem){
        m_totalObjem -= storedNapoj ->getObjem();
    }
    if (storedNapoj ->getCena() < m_totalCena ){
        m_totalCena -= storedNapoj ->getCena();
    }
    m_pocetKusu--;
}

/*
 *  Poslední metodou je metoda printInfo vypisující obsah kontejneru na obrazovku (1 bod).
 * */
void Automat::printInfo(){
    cout << "Celkova cena: " << m_totalCena << endl;
    cout << "Celkovy Objem: " << m_totalObjem << endl;
    cout << "Pocet kusu v automatu: " << m_pocetKusu << endl;
}
