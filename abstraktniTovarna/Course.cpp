//
// Created by xostrize on 29.11.2019.
//

#include "Course.h"


Course::Course(TypStudia typ, std::string name, int credits){
    m_type=typ;
    m_name=name;
    m_credits=credits;
}
TypStudia Course::getTyp(){
    return m_type;
}
int Course::getCredits(){
    return m_credits;
}
std::string Course::getName(){
    return m_name;
}