//
// Created by xostrize on 29.11.2019.
//

#include "Student.h"



Student::Student(std::string name, float scholarchipPerYear, int standardStudyLenght, bool mealDiscount){
    m_name=name;
    m_standardStudyLenght=standardStudyLenght;
    m_scholarshipPerYear=scholarchipPerYear;
    m_mealDiscount=mealDiscount;
}
std::string Student::getName(){
    return m_name;
}

Student* Student::createStudent(std::string name, TypStudia study){
    Student* student=nullptr;

    switch (study){
        case TypStudia::Bc:
            student= new Student(name, 1000, 3, true);
            break;
        case TypStudia::Ing:
            student= new Student(name, 2000, 2, true);
            break;
        case TypStudia::PhD:
            student= new Student(name, 3000, 3, true);
            break;
    }
    return student;
}
float Student::getScholarShipPerYear(){
    return m_scholarshipPerYear;
}
int Student::getStandardStudyLength(){
    return m_standardStudyLenght;
}
bool Student::getMealDiscount(){
    return m_mealDiscount;
}



