//
// Created by xostrize on 29.11.2019.
//

#ifndef ABSTRAKTNITOVARNA_STUDENT_H
#define ABSTRAKTNITOVARNA_STUDENT_H

#include <iostream>
#include "TypStudia.h"

class Student {
private:
    std::string m_name;
    float m_scholarshipPerYear;
    int m_standardStudyLenght;
    bool m_mealDiscount;

    Student(std::string name, float scholarchipPerYear, int standardStudyLenght, bool mealDiscount);
public:
    static Student* createStudent(std::string name, TypStudia study);
    std::string getName();
    float getScholarShipPerYear();
    int getStandardStudyLength();
    bool getMealDiscount();
};


#endif //ABSTRAKTNITOVARNA_STUDENT_H
