//
// Created by xostrize on 29.11.2019.
//

#ifndef ABSTRAKTNITOVARNA_COURSE_H
#define ABSTRAKTNITOVARNA_COURSE_H

#include <iostream>
#include "TypStudia.h"

class Course {
private:
    TypStudia m_type;
    std::string m_name;
    int m_credits;
public:
    Course(TypStudia typ, std::string name, int credits);
    TypStudia getTyp();
    int getCredits();
    std::string getName();

};


#endif //ABSTRAKTNITOVARNA_COURSE_H
