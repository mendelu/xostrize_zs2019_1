//
// Created by xostrize on 29.11.2019.
//

#ifndef ABSTRAKTNITOVARNA_BECHALORFACTORY_H
#define ABSTRAKTNITOVARNA_BECHALORFACTORY_H

#include "Student.h"
#include "Course.h"
#include "StudyFactory.h"

class BechalorFactory: public StudyFactory {
public:

    Student *createStudent(std::string name) override;

    Course *createCourse(std::string name, int credits) override;
};


#endif //ABSTRAKTNITOVARNA_BECHALORFACTORY_H
