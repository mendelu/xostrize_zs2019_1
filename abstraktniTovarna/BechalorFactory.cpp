//
// Created by xostrize on 29.11.2019.
//

#include "BechalorFactory.h"

Student *BechalorFactory::createStudent(std::string name) {
    return Student::createStudent(name, TypStudia::Bc);
}

Course *BechalorFactory::createCourse(std::string name, int credits) {
    return new Course(TypStudia::Bc, name, credits);
}
