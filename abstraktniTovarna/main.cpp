
#include <iostream>

#include "StudyFactory.h"
#include "BechalorFactory.h"

int main() {

    StudyFactory* factory = new BechalorFactory();

    Student* bcStudent= factory->createStudent("Frantisek");
    Course* bcCourse = factory->createCourse("ZOO", 6);

    std::cout<< bcCourse->getName() << std::endl;

    if(TypStudia::Bc== bcCourse->getTyp()){
        std::cout<< "Bakalarske studium" << std::endl;
    }

    delete(bcCourse);
    delete(bcStudent);
    delete(factory);
    return 0;
}