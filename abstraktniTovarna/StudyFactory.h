//
// Created by xostrize on 29.11.2019.
//

#ifndef ABSTRAKTNITOVARNA_STUDYFACTORY_H
#define ABSTRAKTNITOVARNA_STUDYFACTORY_H

#include "Student.h"
#include "Course.h"

class StudyFactory{
public:
    virtual Student* createStudent(std::string name)=0;
    virtual Course* createCourse(std::string name, int credits)=0;
    virtual ~StudyFactory(){};

};

#endif //ABSTRAKTNITOVARNA_STUDYFACTORY_H
