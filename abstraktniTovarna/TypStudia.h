//
// Created by xostrize on 29.11.2019.
//

#ifndef ABSTRAKTNITOVARNA_TYPSTUDIA_H
#define ABSTRAKTNITOVARNA_TYPSTUDIA_H

enum class TypStudia{
    Bc, Ing, PhD,

};

#endif //ABSTRAKTNITOVARNA_TYPSTUDIA_H
