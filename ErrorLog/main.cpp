#include <iostream>

using namespace std;
/*
 * Samostatná práce na cca 5-8 minut model
 * 10 minut zakladni kod
 *
 * Mějme jednoduchou gamesu ve stylu might and magic.
 * budou zde draci a rytíři
 *
 * rytíři budou reprezentování jmenem, zivoty, sílou, a obranou
 * draci budou reprezentování zivoty, sílou a obranou
 *
 * vysledek naimplementujete základní metody get/set/printInfo/uberZivoty atp.
 *
 * v hlavní funkci programu vytvořte po jedne instanci zavolejte "nějakou metodu"
 * a instanci zničte
 *
 * */

class ErrorLog{
private:
    string m_error;
    static ErrorLog* s_log;

    ErrorLog(){
        m_error = "Error log: \n";
    }
public:
    static ErrorLog* getErrorLog(){
        if(s_log == nullptr){
            s_log=new ErrorLog();
        }

        return s_log;

    }

    void logInfo(string where, string infoParametr){
        m_error += "INFO - " + where +" : " + infoParametr+"\n";
    }

    string getInfoLog(){
        return m_error;
    }
};

ErrorLog* ErrorLog::s_log = nullptr;

class Drak{
private:
    int m_zivot;
    int m_obrana;
    int m_utok;
public:
    Drak(int zivot,int  obrana, int utok){
        ErrorLog::getErrorLog()->logInfo("Drak", "vznik draka");
        setObrana(obrana);
        setUtok(utok);
        m_zivot= zivot;
    }
    Drak(int obrana, int utok){
        setObrana(obrana);
        setUtok(utok);
    }

    ~Drak(){
        cout<< "Drak umrel" << endl;
    }

    void setObrana(int obrana){
        m_obrana=obrana;
    }

    void setUtok(int utok){
        m_utok = utok;
    }

    int getObrana(){
        return m_obrana;
    }

    int getUtok(){
        return m_utok;
    }

    int getZivoty(){
        return m_zivot;
    }

    void printInfo(){
        cout<< "Drak ------------------" << endl;
        cout<< "zivoty draka: \t" << getZivoty() << endl;
        cout<< "obrana draka: \t" << getObrana() << endl;
        cout<< "utok draka: \t" << getUtok() << endl;
    }

};

class Rytir{
private:
    string m_jmeno;
    int m_zivot;
    int m_obrana;
    int m_utok;
public:
    Rytir(string jmeno, int zivot,int  obrana, int utok){
        setObrana(obrana);
        setUtok(utok);
        m_zivot= zivot;
        m_jmeno = jmeno;
    }
    Rytir(string jmeno, int obrana, int utok){
        m_jmeno = jmeno;
        setObrana(obrana);
        setUtok(utok);
    }

    ~Rytir(){
        cout<< "Rytir umrel" << endl;
    }

    void setObrana(int obrana){
        m_obrana=obrana;
    }

    void setUtok(int utok){
        m_utok = utok;
    }

    int getObrana(){
        return m_obrana;
    }

    int getUtok(){
        return m_utok;
    }

    int getZivoty(){
        return m_zivot;
    }

    string getJmeno(){
        return m_jmeno;
    }

    void printInfo(){
        cout<< "Rytir ------------------" << endl;
        cout<< "jmeno rytire: \t" << getJmeno() << endl;
        cout<< "zivoty rytir: \t" << getZivoty() << endl;
        cout<< "obrana rytir: \t" << getObrana() << endl;
        cout<< "utok rytire: \t" << getUtok() << endl;
    }

    void uberZivot(int kolik){
        if(getZivoty()> kolik){
            //m_zivot-=kolik;
            m_zivot= m_zivot-kolik;
        }else{
            m_zivot = 0;
            cout<< "Snazis se ubrat vic nastavuji na 0" << endl;
        }
    }

    void bojuj(Drak* sKym){
        if(getObrana() > sKym->getUtok()){
            cout<< "Rytir se ubranil" << endl;
        }else{
            uberZivot(sKym->getUtok());
            cout<< "Rytir se NEubranil" << endl;
        }

    }

};


int main() {
    Rytir* sandokan = new Rytir("sandokan", 100,40,100);
    Drak* pecival = new Drak(100,100,50);
    cout<< "Pocatecni stav" << endl;

    pecival -> printInfo();
    sandokan->printInfo();

    //vezmu ukazatel a zavolam metodu
    //do metody strcim integer ktery mi vrati metoda draka
    //sandokan->uberZivot(pecival->getUtok());
    cout<< "#########" << endl;
    sandokan->bojuj(pecival);

    cout<< "Koncovy stav" << endl;
    pecival -> printInfo();
    sandokan->printInfo();

    delete pecival;
    delete sandokan;

    cout<<ErrorLog::getErrorLog()->getInfoLog();

    return 0;
}