//
// Created by xostrize on 22.11.2019.
//

#include "Zamestnanec.h"

Zamestnanec::Zamestnanec(int letVeFirme, int stupenVzdelani,TypPozice pracovniPozice){
    m_letVeFirme=letVeFirme;
    m_stupenVzdelani=stupenVzdelani;
    m_cerpanoDniDovolene=0;

    m_pozice= nullptr;
    zmenPracovniPozici(pracovniPozice);
}
Zamestnanec::~Zamestnanec(){
    delete m_pozice;
}

int Zamestnanec::getPlat(){
    return m_pozice->getPlat(m_letVeFirme, m_stupenVzdelani);
}
int Zamestnanec::getZbyvaDovolene(){
    return m_pozice->getDniDovolene(m_cerpanoDniDovolene);
}
void Zamestnanec::evidujDovolenou(int cerpaniDni){
    m_cerpanoDniDovolene += cerpaniDni;
}

void Zamestnanec::zmenPracovniPozici(TypPozice pracovniPozice){
    if(m_pozice != nullptr){
        delete m_pozice;
    }

    /*
    if(pracovniPozice == TypPozice::Akademik){
        m_pozice= new Akademik();
    }
    if(pracovniPozice == TypPozice::Technik){
        m_pozice= new Technik();
    }*/

    switch (pracovniPozice){
        case TypPozice::Technik :
            m_pozice= new Technik();
            break;
        case TypPozice::Akademik :
            m_pozice= new Akademik();
            break;
        default:
            std::cout<< "neznama pozice" << std::endl;
            m_pozice= nullptr;
    }
}