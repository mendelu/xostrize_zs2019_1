//
// Created by xostrize on 22.11.2019.
//

#include "Technik.h"

int Technik::getDniDovolene(int cerpani) {
    const int maxDniDovolene =30;
    return  maxDniDovolene-cerpani;
}

int Technik::getPlat(int letVeFirme, int studentVzdelani) {
    const int zakladniPlat = 30000;
    const int rocniBonus= 1000;
    return zakladniPlat+letVeFirme*rocniBonus;
}
