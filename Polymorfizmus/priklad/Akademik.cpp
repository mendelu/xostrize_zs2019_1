//
// Created by xostrize on 22.11.2019.
//

#include "Akademik.h"

int Akademik::getPlat(int letVeFirme, int studentVzdelani) {
    const int zakladniPlat = 40000;
    const int rocniBonus= 5000;
    return zakladniPlat+studentVzdelani*rocniBonus;
}

int Akademik::getDniDovolene(int cerpani) {
    const int maxDniDovolene =50;
    return  maxDniDovolene-cerpani;
}
