//
// Created by xostrize on 22.11.2019.
//

#ifndef PRIKLAD_ZAMESTNANEC_H
#define PRIKLAD_ZAMESTNANEC_H

#include "PracovniPozice.h"
#include "TypPozice.h"
#include "Akademik.h"
#include "Technik.h"
#include <iostream>

class Zamestnanec {
private:
    int m_cerpanoDniDovolene;
    int m_letVeFirme;
    int m_stupenVzdelani;

    PracovniPozice* m_pozice;
public:
    Zamestnanec(int letVeFirme, int stupenVzdelani,TypPozice pracovniPozice);
    ~Zamestnanec();

    int getPlat();
    int getZbyvaDovolene();
    void evidujDovolenou(int cerpaniDni);

    void zmenPracovniPozici(TypPozice pracovniPozice);

};


#endif //PRIKLAD_ZAMESTNANEC_H
