//
// Created by xostrize on 22.11.2019.
//

#ifndef PRIKLAD_TECHNIK_H
#define PRIKLAD_TECHNIK_H

#include "PracovniPozice.h"

class Technik : public  PracovniPozice{
public:
    //ctrl+i klavesova zkratka pro generovani metod a definic
    int getDniDovolene(int cerpani) override;
    int getPlat(int letVeFirme, int studentVzdelani) override;
};

#endif //PRIKLAD_TECHNIK_H
