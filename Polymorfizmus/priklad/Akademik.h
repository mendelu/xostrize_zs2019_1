//
// Created by xostrize on 22.11.2019.
//

#ifndef PRIKLAD_AKADEMIK_H
#define PRIKLAD_AKADEMIK_H

#include "PracovniPozice.h"
class Akademik : public PracovniPozice {
public:
    //ctrl+i klavesova zkratka pro generovani metod a definic
    int getPlat(int letVeFirme, int studentVzdelani) override;
    int getDniDovolene(int cerpani) override;
};


#endif //PRIKLAD_AKADEMIK_H
