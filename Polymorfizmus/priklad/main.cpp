#include <iostream>
#include "Zamestnanec.h"
#include "TypPozice.h"

int main() {
    Zamestnanec* zamestnanec= new Zamestnanec(5,4, TypPozice::Technik);
    std::cout<< "Vyplata: \t" << zamestnanec->getPlat()<< std::endl;
    std::cout<< "Dovolena: \t" << zamestnanec->getZbyvaDovolene()<< std::endl;
    std::cout<< "cerpa dovolenou" << std::endl;
    zamestnanec->evidujDovolenou(5);
    zamestnanec->zmenPracovniPozici(TypPozice::Akademik);
    std::cout<< "Zamestnanec zmenil pracovni pozici" << std::endl;
    std::cout<< "Vyplata: \t" << zamestnanec->getPlat()<< std::endl;
    std::cout<< "Dovolena: \t" << zamestnanec->getZbyvaDovolene()<< std::endl;
    delete(zamestnanec);

    return 0;
}