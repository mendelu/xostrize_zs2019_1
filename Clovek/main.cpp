#include <iostream>

using namespace std;

class Clovek{
public:
    string m_jmeno;
    float m_vaha;
    float m_vyska;
    int m_vek;

    void setVyska(float vyska){
        if(vyska<0){
            cout<<"Nikdo neni mensi nez trpaslik!" << endl;
        }else{
            m_vyska=vyska;
        }

    }
    void setVaha(float vaha){
        if(vaha>0){
            m_vaha=vaha;
        }else{
            cout<<"Nikdo neni lehci nez pirko!" << endl;
        }
    }
    void setJmeno(string jmeno){
        m_jmeno=jmeno;
    }
    /**
     * Tahle metoda prebere vek zkontroluje ho na nezaporna cisla
     * a nastavi vek
     *
     * @param vek - nezaporna hodnota predstavujici vek osoby
     */
    void setVek(int vek){
        if(vek>0){
            m_vek = vek;
        }else{
            cout<< "nikdo neni mladsi nez datum narozeni!" << endl;
        }
    }

    /**
     *
     * @return vraci vysku dane osoby
     */

    float getVyska(){
        return m_vyska;
    }

    int getVek(){
        return m_vek;
    }

    float getVaha(){
        return m_vaha;
    }

    string getJmeno(){
        return m_jmeno;
    }

    /**
     * Tahle metoda bude vypisovat pouze infomace na konzoli.
     */

    void printInfo(){
        cout<< "Clovicek -------------" << endl;
        cout<< "Jmeno osoby je: \t" << m_jmeno << endl;
        cout<< "Vyska osoby je: \t" << m_vyska << endl;
        cout<< "Vaha osoby je: \t\t" << m_vaha << endl;
        cout<< "Vek osoby je: \t\t" << m_vek << endl;
        cout<< "pepovo BMI je: \t\t "<< getBMI() << endl;
    }

    float getBMI(){
        return (m_vyska*m_vyska)/m_vaha;
    }
};


int main() {
    Clovek* pepa = new Clovek();

    pepa->setJmeno("pepa z depa");
    pepa->setVaha(120);
    pepa->setVek(10);
    pepa->setVyska(208);
    pepa->printInfo();


    cout<< "pepovo jmeno je: \t"<< pepa->getJmeno() << endl;
    cout<< "pepovo BMI je: \t "<< pepa->getBMI() << endl;
    delete pepa;
    return 0;
}